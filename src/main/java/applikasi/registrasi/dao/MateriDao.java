package applikasi.registrasi.dao;

import applikasi.registrasi.entity.Materi;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MateriDao extends PagingAndSortingRepository<Materi, String> {
}
